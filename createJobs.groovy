pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}


pipelineJob('theme-park-job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://benzidane_karim@bitbucket.org/benzidane_karim/myapp.git'
                    }
                    branch 'master'
                }
            }
        }
    }
}


pipelineJob('theme-park-job_Docker') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://benzidane_karim@bitbucket.org/benzidane_karim/myapp.git'
                    }
                    branch 'master'
		    scriptPath('Jenkinsfile-docker')
                }
            }
        }
    }
}


pipelineJob('theme-park-job_Docker-run') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://benzidane_karim@bitbucket.org/benzidane_karim/myapp.git'
                    }
                    branch 'master'
                    scriptPath('Jenkinsfile-docker-run')
                }
            }
        }
    }
}
